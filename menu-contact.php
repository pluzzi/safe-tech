<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm navbar-dark" href="index.php">Safe <span>Tech</span></a>
        </div> <!-- navbar-header -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="index.php#inicio" data-hover="dropdown">Inicio</a>
                </li>
                <li class="">
                    <a href="index.php#quehacemos" data-hover="dropdown">Que Hacemos</a>
                </li>
                <li class="">
                    <a href="index.php#productos" data-hover="dropdown">Productos y Servicios</a>
                </li>
                <!--<li class="#protfolio">
                    <a href="" data-hover="dropdown">Portfolio</a>
                </li>-->
                <li class="">
                    <a href="index.php#nosotros" data-hover="dropdown">Nosotros</a>
                </li>                
                <li class="">
                    <a href="index.php#planes" data-hover="dropdown">Planes</a>
                </li>
                <li class="">
                    <a href="index.php#equipo" data-hover="dropdown">Nuestro Equipo</a>
                </li>
                <li class="">
                    <a href="contact.php" data-hover="dropdown">Contact</a>
                </li>
               
            </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>