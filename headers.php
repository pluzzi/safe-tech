<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<title>Safe-Tech</title>

<link rel="shortcut icon" href="assets/img/favicon.png" />

<meta name="description" content="">

<link href="assets/css/preload.css" rel="stylesheet">
<link href="assets/css/vendors.css" rel="stylesheet">
<link href="assets/css/syntaxhighlighter/shCore.css" rel="stylesheet" >
<link href="assets/css/style-blue.css" rel="stylesheet" title="default">
<link href="assets/css/width-full.css" rel="stylesheet" title="default">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139414892-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-139414892-1');
</script>
