<section class="wrap-primary-color margin-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="text-center">
                    <div class="circle" id="circles-1"></div>
                    <h4 class="text-center">HTML 5</h4>
                    <p class="small-font">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="text-center">
                    <div class="circle" id="circles-2"></div>
                    <h4 class="text-center">CSS 3</h4>
                    <p class="small-font">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="text-center">
                    <div class="circle" id="circles-3"></div>
                    <h4 class="text-center">Jquery</h4>
                    <p class="small-font">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="text-center">
                    <div class="circle" id="circles-4"></div>
                    <h4 class="text-center">Bootstrap 3</h4>
                    <p class="small-font">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                </div>
            </div>
    </div>
</section>