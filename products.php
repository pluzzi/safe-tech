<p class="lead lead-lg text-center primary-color wow fadeIn animation-delay-3">Productos y Servicios</p>
<!--<p class="lead lead-sm text-center margin-bottom wow fadeIn animation-delay-5">Put here a short description or brief highlights in your app.</p>-->

<section class="margin-bottom margin-top">
    <ul class="nav nav-pills nav-justified ar-nav-pills max-width-500 center-block margin-bottom">
        <?php
            include('config/database-config.php');
            $sql = "select * from products";
            $result = $conn->query($sql);

            $primero = true;

            while($row = mysqli_fetch_assoc($result)){
                if($primero){
                    echo '<li class="active"><a href="#product'.$row['id'].'" data-toggle="tab"><i class="'.$row['icon'].'"></i> '.$row['description'].'</a></li>';
                    $primero = false;
                }else{
                    echo '<li><a href="#product'.$row['id'].'" data-toggle="tab"><i class="'.$row['icon'].'"></i> '.$row['description'].'</a></li>';
                }
            }

        ?>
    </ul>

    <div class="tab-content margin-top">
        <?php
            include('config/database-config.php');
            $sql = "select * from products";
            $products = $conn->query($sql);

            $primero = true;

            while($product = mysqli_fetch_assoc($products)){
                $sql = "select * from product_details where productid='".$product['id']."'";
                $details = $conn->query($sql);

                if($primero){
                    echo '<div class="tab-pane active" id="product'.$product['id'].'">
                            <div class="row">
                                <div class="col-md-6 col-lg-5 col-md-push-6 col-lg-push-7">
                                    <ul class="list-unstyled hand-list">';
                    $primero = false;
                }else{
                    echo '<div class="tab-pane" id="product'.$product['id'].'">
                            <div class="row">
                                <div class="col-md-6 col-lg-5 col-md-push-6 col-lg-push-7">
                                    <ul class="list-unstyled hand-list">';
                }
                

                while($detail = mysqli_fetch_assoc($details)){
                    echo '<li class="animated fadeInRight animation-delay-2">
                            <h2 class="handwriting no-margin">'.$detail['title'].'</h2>
                            <p class="lead-hand" style="word-wrap: break-word;">'.$detail['description'].'</p>
                        </li>';
                }

                echo '          </ul>
                            </div>
                            <div class="col-md-6 col-lg-7 col-md-pull-6 col-lg-pull-5">
                                <img class="img-responsive animated zoomInDown animation-delay-3" src="data:image/jpeg;base64,'.base64_encode( $product['img'] ).'" />
                            </div>
                        </div>
                    </div>';
            }

        ?>

    </div>
</section>
</div> <!-- container -->