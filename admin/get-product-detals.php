<?php
    include('config/database-config.php');
        
    $sql = "select d.id, d.title, d.description, p.description as product
    from product_details d
    left join products p on d.productid=p.id
    where d.productid='".$_GET['id']."'";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['product'] .'</td>
                <td>'.$row['title'] .'</td>
                <td>'.$row['description'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }

?>