<?php
    include('config/database-config.php');
        
    $sql = "select b.id, b.description, c.name, b.approved from budgets b left join clients c on c.id=b.clientid";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $sql = "select
        round(sum(a.price*c.rate + a.price*c.rate*a.iva/100),4) as iva
        from budget_details d
        left join articles a on a.id=d.articleid
        left join currency c on c.id=a.currencyid
        where d.budgetid='".$row['id']."'";

        $total = $conn->query($sql);
        $valor = mysqli_fetch_assoc($total);

        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['description'].'</td>
                <td>$ '.$valor['iva'] .'</td>
                <td>$ '.$row['approved'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="details" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-list-ul"></i>
                    </button>
                    <button id="approve" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-check"></i>
                    </button>
                </td>
            </tr>';
    }

?>