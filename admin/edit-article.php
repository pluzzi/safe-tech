<?php include('islogin.php'); ?>

<?php
    $page_id = 6;

    include('config/database-config.php');
        
    $sql = "select * from articles where id=".$_GET['id'];
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Editar Artículo</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                            <div class="form-group"><label class="col-sm-2 control-label">Código</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="code" value="<?php echo $row['code']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="name" value="<?php echo $row['name']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description" value="<?php echo $row['description']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Precio</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="price" value="<?php echo $row['price']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">IVA</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="iva" value="<?php echo $row['iva']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Moneda</label>
                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="currency">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from currency";
                                                $monedas = $conn->query($sql);
                                    
                                                while ($moneda = mysqli_fetch_assoc($monedas)) {
                                                    if($moneda['id']==$row['currencyid']){
                                                        echo '<option selected data-id="'.$moneda['id'].'">'.$moneda['description'].'</option>';
                                                    }else{
                                                        echo '<option data-id="'.$moneda['id'].'">'.$moneda['description'].'</option>';
                                                    }
                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo</label>
                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="articletype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from article_types";
                                                $types = $conn->query($sql);
                                    
                                                while ($type = mysqli_fetch_assoc($types)) {
                                                    if($type['id']==$row['articletype']){
                                                        echo '<option selected data-id="'.$type['id'].'">'.$type['description'].'</option>';
                                                    }else{
                                                        echo '<option data-id="'.$type['id'].'">'.$type['description'].'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function update_data(value, column){
            var $_GET = <?php echo json_encode($_GET); ?>;
            
            $.ajax({
                url: "update-article.php",
                method: "POST",
                data: {valor: value, columna: column, id: $_GET['id']},
                success: function(results){
                    //alert(results);
                }
            });
        }

        $(document).on("blur", "#code", function(){
            var code = $(this).val();
            update_data(code, "code");
        })
        $(document).on("blur", "#name", function(){
            var name = $(this).val();
            update_data(name, "name");
        })
        $(document).on("blur", "#description", function(){
            var description = $(this).val();
            update_data(description, "description");
        })
        $(document).on("blur", "#price", function(){
            var price = $(this).val();
            update_data(price, "price");
        })
        $(document).on("blur", "#iva", function(){
            var iva = $(this).val();
            update_data(iva, "iva");
        })
        $(document).on("change", "#currency", function(){
            var currency = $(this).children(":selected").data("id");
            update_data(currency, "currencyid");
        })
        $(document).on("change", "#articletype", function(){
            var articletype = $(this).children(":selected").data("id");
            update_data(articletype, "articletype");
        })

        $(document).ready(function() {
            
        });
    </script>

</body>

</html>