<?php
    include('config/database-config.php');
        
    $sql = "select * from users";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td contenteditable="true" id="nameUpdate" data-id="'.$row['id'].'">'.$row['name'] .'</td>
                <td contenteditable="true" id="emailUpdate" data-id="'.$row['id'].'">'.$row['email'] .'</td>
                <td contenteditable="true" id="passwordUpdate" class="hidetext" data-id="'.$row['id'].'">'.$row['password'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }

    echo '<tr class="table-light">
            <td id="idAdd"></td>
            <td id="nameAdd" contenteditable="true"></td>
            <td id="emailAdd" contenteditable="true"></td>
            <td id="passwordAdd" class="hidetext" contenteditable="true"></td>
            <td>
                <button id="agregar" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus-square"></i>
                </button>
            </td>
        </tr>';
?>
