<?php
    include('config/database-config.php');
        
    $sql = "select m.id, c.name, c.email, m.title, m.create_date from messages m left join contacts c on c.id = m.contactid order by create_date desc";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['email'] .'</td>
                <td>'.$row['title'] .'</td>
                <td>'.$row['create_date'] .'</td>
                <td>
                    <button id="read" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
            </tr>';
    }
?>
