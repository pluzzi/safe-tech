<?php
    include('config/database-config.php');
        
    $sql = "select id, taxid, name from clients";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['taxid'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="contact" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-address-card"></i>
                    </button>
                </td>
            </tr>';
    }

?>