<?php include('islogin.php'); ?>

<?php
    $page_id = 6;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Artículo</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Código</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="code"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="name"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Precio</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="price"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">IVA</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="iva"></div>
                                </div>

                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Moneda</label>
                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="currency">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from currency";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo</label>
                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="articletype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from article_types";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        
        $(document).ready(function() {
            $(document).on("click", "#save", function(){
                var code = $("#code").val();
                var name = $("#name").val();
                var description = $("#description").val();
                var price = $("#price").val();
                var iva = $("#iva").val();
                var currencyid = $("#currency").children(":selected").data("id");
                var articletype = $("#articletype").children(":selected").data("id");

                $.ajax({
                    url: "save-article.php",
                    method: "POST",
                    data: {code: code, name: name, description: description, price: price, iva: iva, currencyid: currencyid, articletype: articletype},
                    success: function(result){
                        window.location.href = "articles.php";
                    }
                });
            })

        });
    </script>

</body>

</html>