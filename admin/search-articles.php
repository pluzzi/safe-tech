<?php
    include('config/database-config.php');
        
    $sql = "select a.id, a.code, t.description, a.name, c.simbol, a.price, a.iva
    from articles a
    left join currency c on c.id=a.currencyid
    left join article_types t on t.id=a.articletype
    where a.name like '%".$_POST['like']."%'
    or a.description like '%".$_POST['like']."%'
    or a.code like '%".$_POST['like']."%'";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['code'] .'</td>
                <td>'.$row['description'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['simbol'] .'</td>
                <td>'.$row['price'] .'</td>
                <td>'.$row['iva'] .'</td>
                <td>
                    <button id="add-article" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-plus-square"></i>
                    </button>
                </td>
            </tr>';
    }

?>