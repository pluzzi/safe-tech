<?php include('islogin.php'); ?>

<?php
    $page_id = 7;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Presupuesto</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cliente</label>
                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="client">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from clients";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['name'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        
        $(document).ready(function() {
            $(document).on("click", "#save", function(){
                var clientid = $("#client").children(":selected").data("id");
                var description = $("#description").val();
                
                $.ajax({
                    url: "save-budget.php",
                    method: "POST",
                    data: {clientid: clientid, description: description},
                    success: function(id){
                        window.location.href = "budget.php";
                    }
                });
            })

        });
    </script>

</body>

</html>