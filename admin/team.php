<?php include('islogin.php'); ?>

<?php
    $page_id = 4;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Equipo</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <button id="add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus-square"></i>
                            </button>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Profesión</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getTeam(){
            $.ajax({
                url: "get-team.php",
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-team.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getTeam();
                    }
                })
            })

            $(document).on("click", "#add", function(){
                window.location.href = "add-team.php";
            })

            $(document).on("click", "#edit", function(){
                var id = $(this).data("id");

                window.location.href = "edit-team.php?id="+id;
            })

            getTeam();

        });
    </script>

</body>

</html>