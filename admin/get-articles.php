<?php
    include('config/database-config.php');
        
    $sql = "select a.id, a.code, t.description, a.name, c.simbol, a.price, a.iva
    from articles a
    left join currency c on c.id=a.currencyid
    left join article_types t on t.id=a.articletype";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['code'] .'</td>
                <td>'.$row['description'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['simbol'] .'</td>
                <td>'.$row['price'] .'</td>
                <td>'.$row['iva'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
            </tr>';
    }

?>