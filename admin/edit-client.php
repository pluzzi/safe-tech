<?php include('islogin.php'); ?>

<?php
    $page_id = 5;

    include('config/database-config.php');
        
    $sql = "select id, name, taxidtype, taxid from clients where id=".$_GET['id'];
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Editar Cliente</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="name" value="<?php echo $row['name']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tax Id Type</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="taxidtype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from taxid_types";
                                                $tipos = $conn->query($sql);
                                    
                                                while ($tipo = mysqli_fetch_assoc($tipos)) {
                                                    if($tipo['id']==$row['taxidtype']){
                                                        echo '<option selected data-id="'.$tipo['id'].'">'.$tipo['description'].'</option>';
                                                    }else{
                                                        echo '<option data-id="'.$tipo['id'].'">'.$tipo['description'].'</option>';
                                                    }
                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Tax Id</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="taxid" value="<?php echo $row['taxid']; ?>"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function update_data(value, column){
            var $_GET = <?php echo json_encode($_GET); ?>;
            
            $.ajax({
                url: "update-client.php",
                method: "POST",
                data: {valor: value, columna: column, id: $_GET['id']},
                success: function(results){
                    //alert(results);
                }
            });
        }

        $(document).on("blur", "#name", function(){
            var name = $(this).val();
            update_data(name, "name");
        })
        $(document).on("blur", "#taxid", function(){
            var taxid = $(this).val();
            update_data(taxid, "taxid");
        })
        $(document).on("change", "#taxidtype", function(){
            var taxidtype = $(this).children(":selected").data("id");
            update_data(taxidtype, "taxidtype");
        })

        $(document).ready(function() {
            
        });
    </script>

</body>

</html>