<?php include('islogin.php'); ?>

<?php
    $page_id = 5;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Direcciones del Cliente</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Tipo</th>
                                            <th>Dirección</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="address-result">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de Dirección</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="addresstype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from address_types";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Dirección</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="address"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save-address" >Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Teléfonos del Cliente</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Tipo</th>
                                            <th>Número</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="phone-result">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de Teléfono</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="phonetype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from phone_types";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Número</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="number"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save-phone" >Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getAddress(){
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: "get-client-address.php?id="+id,
                method: "POST",
                success: function(results){
                    $("#address-result").html(results);
                }
            })
        }
        function getPhone(){
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: "get-client-phone.php?id="+id,
                method: "POST",
                success: function(results){
                    $("#phone-result").html(results);
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            $(document).on("click", "#delete-address", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-contact-address.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getAddress();
                    }
                })
            })
            $(document).on("click", "#delete-phone", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-contact-phone.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getPhone();
                    }
                })
            })

            $(document).on("click", "#save-address", function(){
                var addrestype = $("#addresstype").children(":selected").data("id");
                var address = $("#address").val();
                var clientid = '<?php echo $_GET['id'] ?>';

                $.ajax({
                    url: "save-contact-address.php",
                    method: "POST",
                    data: {addrestype: addrestype, address: address, clientid: clientid},
                    success: function(result){
                        $("#address").val("");
                        getAddress();
                    }
                });
            });

            $(document).on("click", "#save-phone", function(){
                var phonetype = $("#phonetype").children(":selected").data("id");
                var number = $("#number").val();
                var clientid = '<?php echo $_GET['id'] ?>';

                $.ajax({
                    url: "save-contact-phone.php",
                    method: "POST",
                    data: {phonetype: phonetype, number: number, clientid: clientid},
                    success: function(result){
                        $("#number").val("");
                        getPhone();
                    }
                });
            });

            getAddress();
            getPhone();

        });
    </script>

</body>