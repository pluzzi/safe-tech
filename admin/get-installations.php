<?php
    include('config/database-config.php');
        
    $sql = "select 
    b.id, b.description, c.name, b.approved 
    from installations b 
    left join clients c on c.id=b.clientid";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['description'].'</td>
                <td>$ '.$row['approved'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="details" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-list-ul"></i>
                    </button>
                </td>
            </tr>';
    }

?>