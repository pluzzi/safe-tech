<?php
    include('config/database-config.php');
        
    $sql = "select id, description from products";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['description'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="details" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-list-ul"></i>
                    </button>
                </td>
            </tr>';
    }

?>