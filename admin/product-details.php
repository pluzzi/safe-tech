<?php include('islogin.php'); ?>

<?php
    $page_id = 3;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detalles del Producto</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Producto</th>
                                            <th>Título</th>
                                            <th>Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Título</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="title"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getDetails(){
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: "get-product-detals.php?id="+id,
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-product-detail.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getDetails();
                    }
                })
            })

            $(document).on("click", "#save", function(){
                var title = $("#title").val();
                var description = $("#description").val();
                var product = '<?php echo $_GET['id'] ?>';

                $.ajax({
                    url: "save-product-detail.php",
                    method: "POST",
                    data: {title: title, description: description, product: product},
                    success: function(result){
                        $("#title").val("");
                        $("#description").val("");
                        getDetails();
                    }
                });
            });

            getDetails();

        });
    </script>

</body>

</html>