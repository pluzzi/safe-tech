<?php include('islogin.php'); ?>

<?php
    $page_id = 4;

    include('config/database-config.php');
        
    $sql = "select id, description, icon, img from products where id=".$_GET['id'];
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Editar Producto</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description" value="<?php echo $row['description']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Icon</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="icon" value="<?php echo $row['icon']; ?>"></div>
                                </div>

                                <!-- Cropper -->
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Imagen</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el Producto.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />'; ?>
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input data-id="<?php echo $row['id']; ?>" onchange="readURL(this);" accept="image/*" type="file"  id="product-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                saveImg();
            }
        }

        function update_data(value, column){
            var $_GET = <?php echo json_encode($_GET); ?>;
            
            $.ajax({
                url: "update-product.php",
                method: "POST",
                data: {valor: value, columna: column, id: $_GET['id']},
                success: function(results){
                    //alert(results);
                }
            });
        }

        $(document).on("blur", "#description", function(){
            var description = $(this).val();
            update_data(description, "description");
        })
        $(document).on("blur", "#icon", function(){
            var icon = $(this).val();
            update_data(icon, "icon");
        })

        function saveImg() {
            var file_data = $("#product-img").prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('file', file_data);

            $.ajax({
                url: 'update-product-img.php?id='+$("#product-img").data('id'),
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'POST',
                success: function(php_script_response){
                    //alert(php_script_response); // display response from the PHP script, if any
                }
            });
        }

        $(document).ready(function() {
            
        });
    </script>

</body>

</html>