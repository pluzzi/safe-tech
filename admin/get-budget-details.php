<?php
    include('config/database-config.php');
        
    $sql = "select b.id, a.code, a.name, c.simbol, a.price, a.iva
    from budget_details b
    left join articles a on a.id=b.articleid
    left join currency c on c.id=a.currencyid
    left join article_types t on t.id=a.articletype
    where b.budgetid = '".$_GET['id']."'";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['code'] .'</td>
                <td>'.$row['name'] .'</td>
                <td>'.$row['simbol'] .'</td>
                <td>'.$row['price'] .'</td>
                <td>'.$row['iva'] .'</td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }

?>