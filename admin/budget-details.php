<?php include('islogin.php'); ?>

<?php
    $page_id = 7;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detalles del Presupuesto</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Código</th>
                                            <th>Articulo</th>
                                            <th>Moneda</th>
                                            <th>Precio</th>
                                            <th>IVA</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Buscar Artículo</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="article"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Código</th>
                                                <th>Tipo</th>
                                                <th>Nombre</th>
                                                <th>Moneda</th>
                                                <th>Precio</th>
                                                <th>IVA</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="articles-results">
                                            
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getDetails(){
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: "get-budget-details.php?id="+id,
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");
                
                $.ajax({
                    url: "delete-budget-detail.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getDetails();
                    }
                })
            })

            $(document).on("click", "#add-article", function(){
                var budgetid = '<?php echo $_GET['id']; ?>';
                var articleid = $(this).data("id");

                $.ajax({
                    url: "save-budget-detail.php",
                    method: "POST",
                    data: {budgetid: budgetid, articleid: articleid},
                    success: function(results){
                        getDetails();
                    }
                })
            });

            $(document).on("keyup", "#article", function(){
                var like = $(this).val();
                $.ajax({
                    url: "search-articles.php",
                    method: "POST",
                    data: {like: like},
                    success: function(result){
                        $("#articles-results").html(result);
                    }
                });
            })

            getDetails();

        });
    </script>

</body>

</html>