<?php include('islogin.php'); ?>

<?php
    $page_id = 5;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Cliente</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Nombre/Razon Social</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="name"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de TaxId</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="taxidtype">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from taxid_types";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Tax Id</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="taxid"></div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        
        $(document).ready(function() {
            $(document).on("click", "#save", function(){
                var name = $("#name").val();
                var taxidtype = $("#taxidtype").children(":selected").data("id");
                var taxid = $("#taxid").val();

                $.ajax({
                    url: "save-client.php",
                    method: "POST",
                    data: {name: name, taxidtype: taxidtype, taxid: taxid},
                    success: function(result){
                        window.location.href = "clients.php";
                    }
                });
            })

        });
    </script>

</body>

</html>