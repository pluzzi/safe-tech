<?php include('islogin.php'); ?>

<?php
    $page_id = 9;

    include('config/database-config.php');
        
    $sql = "select id, title, author, create_date, body, img, categoryid from posts where id=".$_GET['id'];
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
    <link href="css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Editar Post</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Título</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="title" value="<?php echo $row['title']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Autor</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="author" value="<?php echo $row['author']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Categoría</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="category">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from post_category";
                                                $categories = $conn->query($sql);
                                    
                                                while ($category = mysqli_fetch_assoc($categories)) {
                                                    if($category['id']==$row['categoryid']){
                                                        echo '<option selected data-id="'.$category['id'].'">'.$category['description'].'</option>';
                                                    }else{
                                                        echo '<option data-id="'.$category['id'].'">'.$category['description'].'</option>';
                                                    }
                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="wrapper wrapper-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Cuerpo del Post</h5>
                                                </div>
                                                <div class="ibox-content no-padding">
                                                    <div class="summernote">
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Cropper -->
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Imagen</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el Post.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <!--<img class="img-preview img-preview-sm" id="preview"/>-->
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />'; ?>
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input onchange="readURL(this);" accept="image/*" type="file"  id="post-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script src="js/plugins/summernote/summernote.min.js"></script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                saveImg();
            }
        }

        function update_data(value, column){
            var $_GET = <?php echo json_encode($_GET); ?>;
            
            $.ajax({
                url: "update-post.php",
                method: "POST",
                data: {valor: value, columna: column, id: $_GET['id']},
                success: function(results){
                    //alert(results);
                }
            });
        }

        $(document).on("blur", "#title", function(){
            var title = $(this).val();
            update_data(title, "title");
        })
        $(document).on("blur", "#author", function(){
            var author = $(this).val();
            update_data(author, "author");
        })
        $(document).on("change", "#category", function(){
            var category = $(this).children(":selected").data("id");
            update_data(category, "category");
        })
        $(".summernote").on("summernote.change", function (e) {
            var body = $('.summernote').summernote('code');
            update_data(body, "body");
        });

        function saveImg() {
            var file_data = $("#post-img").prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('file', file_data);

            var $_GET = <?php echo json_encode($_GET); ?>;

            $.ajax({
                url: 'update-post-img.php?id='+$_GET['id'],
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'POST',
                success: function(php_script_response){
                    //alert(php_script_response); // display response from the PHP script, if any
                }
            });
        }

        $(document).ready(function() {
            $('.summernote').summernote({
                height: 130
            });

            var body = '<?php echo $row['body']; ?>';
            $('.summernote').summernote('code', body);

        });
    </script>

</body>

</html>