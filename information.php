<div class="container margin-bottom">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line no-margin-bottom">Acerca de nosotros</h2>
        </div>
        <div class="col-md-6 animated fadeInLeft animation-delay-8">
            <h3>Safe-Tech</h3>
            <p class="animated">
            Somos una empresa de sistemas de seguridad electrónica especializada en brindar a hogares y comercios
            protección de sus bienes y preservar la integridad física de las personas.
            </p>
            <p>
            Juntos diseñamos la mejor solución que se adapte a las necesidades del cliente, planteando
            soluciones confiables y eficientes.
            </p>
            <p>
            Trabajamos con las mejores marcas del mercado. Asegurando así el mejor servicio.
            Garantizando confianza a nuestros clientes.
            </p>
            <p>
            Estamos expandiendo nuestro mercado hacia la integracion de los sistemas de seguridad electrónica
            junto con las nuevas tecnologías de Internet de las Cosas (IoT).
            </p>
            
        </div>
        <div class="col-md-6">
            <h3>Conocimientos</h3>
            <div class="progress progress-lg animated fadeInUp animation-delay-6">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                Electrónica
            </div>
            </div>
            <div class="progress progress-lg animated fadeInUp animation-delay-8">
            <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                Seguridad
            </div>
            </div>
            <div class="progress progress-lg animated fadeInUp animation-delay-10">
            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                Normas de instalación eléctrica
            </div>
            </div>
            <div class="progress progress-lg animated fadeInUp animation-delay-12">
            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                Monitoreo
            </div>
            </div>
            <div class="progress progress-lg animated fadeInUp animation-delay-14">
            <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                IoT
            </div>
            </div>
            <div class="progress progress-lg animated fadeInUp animation-delay-16">
            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                Programación
            </div>
            </div>
        </div>
    </div>
</div>

<!--<div class="container">
    <section class="css-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line">Some numerical data</h2>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default wow zoomInUp animation-delay-2">
                    <h4 id="count-coffees" class="content-box-title counter">450</h4>
                    <i class="fa fa-4x fa-coffee primary-color"></i>
                    <p class="margin-top-20 no-margin-bottom lead small-caps">cups of coffee</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default wow zoomInUp animation-delay-4">
                    <h4 id="count-coffees" class="content-box-title counter">64</h4>
                    <i class="fa fa-4x fa-briefcase primary-color"></i>
                    <p class="margin-top-20 no-margin-bottom lead small-caps">projects done</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default wow zoomInUp animation-delay-6">
                    <h4 id="count-coffees" class="content-box-title counter">600</h4>
                    <i class="fa fa-4x fa-comments-o primary-color"></i>
                    <p class="margin-top-20 no-margin-bottom lead small-caps">comments</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="content-box box-default wow zoomInUp animation-delay-8">
                    <h4 id="count-coffees" class="content-box-title counter">3500</h4>
                    <i class="fa fa-4x fa-group primary-color"></i>
                    <p class="margin-top-20 no-margin-bottom lead small-caps">happy clients</p>
                </div>
            </div>
        </div>
    </section>
</div> -->