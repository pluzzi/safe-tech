<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('headers.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('top-bar.php'); ?>

            <?php include('menu.php'); ?>

            <a name="inicio" id="inicio"></a>
            <?php include('carousel.php'); ?>

            <a name="quehacemos" id="quehacemos"></a>
            <?php include('what-we-do.php'); ?>

            <a name="productos" id="productos"></a>
            <?php include('products.php'); ?>

            <?php //include('statistics.php'); ?>

            <?php //include('works.php'); ?>

            <a name="nosotros" id="nosotros"></a>
            <?php include('information.php'); ?>

            <a name="planes" id="planes"></a>
            <?php include('plans.php'); ?>

            <a name="equipo" id="equipo"></a>
            <?php include('team.php'); ?>

            <?php include('news-letters.php'); ?>

            <a name="contacto" id="contacto"></a>
            <?php include('contact.php'); ?>

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->

    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

</body>

</html>
