<section class="margin-top">
    <p class="slogan text-center">Discover our projects and the rigorous process of <span>creation</span>. Our principles are <span>creativity</span>, <span>design</span>, <span>experience</span> and <span>knowledge</span>. We are backed by 20 years of <span>research</span>.</p>
    <h2 class="section-title">Our Clients</h2>
    <div class="row">
        <div class="col-md-6">
            <div class="bxslider-controls">
                <span id="bx-prev5"></span>
                <span id="bx-next5"></span>
            </div>
            <ul class="bxslider" id="home-block">
                <li>
                    <blockquote class="blockquote-color">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante ultricies nisi vel augue quam semper libero.</p>
                        <footer>Brian Krzanich, Intel CEO</footer>
                    </blockquote>
                </li>
                <li>
                    <blockquote class="blockquote-color">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante ultricies nisi vel augue quam semper libero.</p>
                        <footer>Brian Krzanich, Intel CEO</footer>
                    </blockquote>
                </li>
                <li>
                    <blockquote class="blockquote-color">
                        <p>Dolore totam at ea reiciendis suscipit a tempore cum nisi aspernatur nisi alias posuere erat a ante posuere erat a ante ultricies ultricies nisi vel augue quam semper conse erat quuntur.</p>
                        <footer>Sheldon Cooper, Physical Quantum</footer>
                    </blockquote>
                </li>
                <li>
                    <blockquote class="blockquote-color">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante ultricies nisi vel augue quam semper libero.</p>
                        <footer>Brian Krzanich, Intel CEO</footer>
                    </blockquote>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/intel.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/microsoft.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/nokia.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/samsung.png" alt="" class="img-responsive"></div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/anz.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/maxis.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/sony.png" alt="" class="img-responsive"></div>
                <div class="col-md-3 col-sm-3 col-xs-6"><img src="assets/img/demo/hp.png" alt="" class="img-responsive"></div>
            </div>
        </div>
    </div>
</section>