<div class="container">
    <section class="margin-bottom">
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line">A qué nos dedicamos?</h2>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-bell"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Intrusión y Robo</h3>
                        <p>Sistema de protección contra intrusión o robo que se adapta a las necesidades de cada cliente. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-fire"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Incendio y Gases</h3>
                        <p>Sistema de detección y aviso de incendio o gases peligrosos.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-facetime-video"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Circuito Cerrado de TV</h3>
                        <p>Sistemas de grabación de video y acceso a través de la nube.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-eye-open"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Control de Accesos</h3>
                        <p>Soluciones destinadas a supervición de acceso a zonas restringidas.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-cloud"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Sistema de Monitoreo Personal</h3>
                        <p>Desarrollo personalizado de sistema basado en la nube para controlar sus sistema de seguridad.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="glyphicon glyphicon-stats"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Monitoreo a través de Central</h3>
                        <p>Trabajamos en conjunto con una empresa especializada en monitoreo de sistemas de seguridad.</p>
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </section>