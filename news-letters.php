<section class="section-lines">
    <div class="container">
        <div class="row">
            <div class="col-md-12 padding-40">
                <p class="slogan text-center no-margin">
                     Mantente informado con nuestro <span> news letter </span>
                </p>
                <p>
                    <form method="POST" action="add-contact-newsletter.php">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email Adress" name="email">
                            <span class="input-group-btn">
                                <button class="btn btn-ar btn-primary" type="submit">Subscribete</button>
                            </span>
                        </div>
                    </form>
                </p>
            </div>
        </div>
    </div>
</section>