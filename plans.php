<div class="container">
    <h2 class="right-line">Planes Pos Instalación</h2>
    <section class="css-section">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricign-box wow fadeInUp animation-delay-7">
                    <div class="pricing-box-header">
                        <h2>Mantenimiento</h2>
                        <p>Soporte técnico</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 749 <sub>/ mes</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="glyphicon glyphicon-check"></i> Pruebas mensuales.</li>
                            <li><i class="glyphicon glyphicon-map-marker"></i> Visita sin costo.</li>
                            <li><i class="glyphicon glyphicon-earphone"></i> Soporte técnico telefónico.</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <!--<a href="#" class="btn btn-ar btn-default">Order Now</a>-->
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="pricign-box wow fadeInUp animation-delay-9">
                    <div class="pricing-box-header">
                        <h2>Monitoreo</h2>
                        <p>Monitoreo a través de Security 24</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 749 <sub>/ mes</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="glyphicon glyphicon-check"></i> Verificación y notificación de reportes</li>
                            <li><i class="glyphicon glyphicon-warning-sign"></i> Aviso a las autoridades</li>
                            <li><i class="glyphicon glyphicon-flash"></i> Aviso falta de corriente o batería baja</li>
                            <li><i class="glyphicon glyphicon-cog"></i> Test de prueba diario con notificación</li>
                            <li><i class="glyphicon glyphicon-cloud"></i> Acceso web y App</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <!--<a href="#" class="btn btn-ar btn-primary">Order Now</a>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricign-box pricign-box-pro wow fadeInUp animation-delay-8">
                    <div class="pricing-box-header">
                        <h2>Full</h2>
                        <p>Mantenimiento y Monitoreo a través de Security 24</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 999 <sub>/ mes</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker"></i> Visita sin costo.</li>
                            <li><i class="glyphicon glyphicon-earphone"></i> Soporte técnico telefónico.</li>
                            <li><i class="glyphicon glyphicon-check"></i> Verificación y notificación de reportes</li>
                            <li><i class="glyphicon glyphicon-warning-sign"></i> Aviso a las autoridades</li>
                            <li><i class="glyphicon glyphicon-flash"></i> Aviso falta de corriente o batería baja</li>
                            <li><i class="glyphicon glyphicon-cog"></i> Test de prueba diario con notificación</li>
                            <li><i class="glyphicon glyphicon-cloud"></i> Acceso web y App</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <!--<a href="#" class="btn btn-ar btn-default">Order Now</a>-->
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </section>
</div>