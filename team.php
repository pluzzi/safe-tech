<div class="container">
    <h2 class="right-line">Nuestro Equipo</h2>
    <div class="row">
        <?php
            include('config/database-config.php');
            $sql = "select * from team limit 3";
            $result = $conn->query($sql);
            $i=1;

            while($row = mysqli_fetch_assoc($result)){
                echo '<div class="col-sm-6 col-md-4">
                        <div class="panel panel-default panel-card wow fadeInRight animation-delay-2">
                            <div class="panel-heading">
                                <img src="assets/img/demo/card'.$i++.'.jpg" />
                            </div>
                            <div class="panel-figure">
                                <img class="img-responsive img-circle" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />
                            </div>
                            <div class="panel-body text-center">
                                <h4 class="panel-header"><a href="">@'.$row['name'].'</a></h4>
                                <small>'.$row['profession'].'</small>
                            </div>
                            <div class="panel-thumbnails">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <a class="social-icon-ar round facebook" href="'.$row['facebook'].'"><i class="fa fa-facebook"></i></a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a class="social-icon-ar round twitter" href="'.$row['twitter'].'"><i class="fa fa-twitter"></i></a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a class="social-icon-ar round linkedin" href="'.$row['linkedin'].'"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

        ?>
        
        
    </div>

    <?php //include('clients.php'); ?>

</div> <!-- container -->