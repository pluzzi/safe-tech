<section class="wrap-hero margin-bottom">
    <div id="carousel-example-ny" class="carousel carousel-hero slide" data-ride="carousel" data-interval="9000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-ny" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-ny" data-slide-to="1"></li>
            <li data-target="#carousel-example-ny" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="">
                                <img src="assets/img/carousel_01.png" alt="" class="img-responsive animated zoomInUp animation-delay-30">
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="carousel-caption">
                                <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">
                                    <span>Seguridad</span> para el Hogar o Comercio
                                </h1>
                                <ul class="list-unstyled carousel-list">
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-11"></i>
                                        <span class="animated fadeInRightBig animation-delay-13">
                                            Asesoramiento profesional y personalizado.
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-15"></i>
                                        <span class="animated fadeInRightBig animation-delay-16">
                                            Marcas internacionales.
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-19"></i>
                                        <span class="animated fadeInRightBig animation-delay-19">
                                            Monitoreo las 24hs.
                                        </span>
                                    </li>
                                </ul>
                                <p class="animated zoomIn animation-delay-20">
                                    Ofrecemos tecnología avanzada de renombre y sevicios profesionales para intrusión de hogar o comercio.
                                </p>
                            </div>
                        </div>
                    </div> <!--row -->
                </div> <!-- container -->
            </div> <!-- item -->

            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="carousel-object">

                                <img src="assets/img/demo/wire1.png" class="img-responsive base animated animated-slow reveal animation-delay-25" alt="Image">
                                <img src="assets/img/demo/wire1.png" class="img-responsive relative animated animated-slow reveal animation-delay-25" alt="Image">
                                <img src="assets/img/demo/wire1shadow.png" class="img-responsive relative animated fadeIn animation-delay-40" alt="Image">
                                <img src="assets/img/carousel_02.png" class="img-responsive relative animated fadeIn animation-delay-45" alt="Image">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="carousel-caption">
                                <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">
                                    <span>Monitoreo</span> personalizado o por central
                                </h1>
                                <ul class="list-unstyled carousel-list">
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-11"></i>
                                        <span class="animated fadeInRightBig animation-delay-13">
                                            Servicio con central de monitoreo Security 24
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-15"></i>
                                        <span class="animated fadeInRightBig animation-delay-16">
                                            Sistema de monitoreo personalizado multidispositivo
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-19"></i>
                                        <span class="animated fadeInRightBig animation-delay-19">
                                            Monitoreo las 24hs.
                                        </span>
                                    </li>
                                </ul>
                                <p class="animated zoomIn animation-delay-20">
                                    Ofrecemos dos tipos de monitoreos, personalizado o a través de una central. Decida que opción se adapta mejor a sus necesidades.
                                </p>
                            </div>
                        </div>
                    </div> <!--row -->
                </div> <!-- container -->
            </div> <!-- item -->

            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="">
                                <img src="assets/img/carousel_03.png" alt="" class="img-responsive animated zoomInRight animation-delay-30">
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="carousel-caption">
                            <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">
                                    <span>Video Vigilancia</span>
                                </h1>
                                <ul class="list-unstyled carousel-list">
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-11"></i>
                                        <span class="animated fadeInRightBig animation-delay-13">
                                            Camaras día y noche
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-15"></i>
                                        <span class="animated fadeInRightBig animation-delay-16">
                                            Almacenamiento de datos
                                        </span>
                                    </li>
                                    <li class="">
                                        <i class="fa fa-angle-right animated fadeIn animation-delay-19"></i>
                                        <span class="animated fadeInRightBig animation-delay-19">
                                            Acceso remoto
                                        </span>
                                    </li>
                                </ul>
                                <p class="animated zoomIn animation-delay-20">
                                    Ofrecemos un sistema personalizado para que la solución se adapte a sus necesidades.
                                </p>
                            </div>
                        </div>
                    </div> <!--row -->
                </div> <!-- container -->
            </div> <!-- item -->
        </div> <!-- carousel-inner -->

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-ny" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#carousel-example-ny" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</section> <!-- carousel -->