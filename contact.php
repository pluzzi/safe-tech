<iframe 
    class="margin-bottom" 
    width="100%" 
    height="350" 
    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3348.763878576395!2d-60.715109584737334!3d-32.93083617841821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b6532581315b8b%3A0x123413e1e75bac32!2sAv.+Provincias+Unidas+217%2C+S2008BQE+Rosario%2C+Santa+Fe!5e0!3m2!1ses-419!2sar!4v1556491829776!5m2!1ses-419!2sar">
</iframe>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="section-title">Enviar Mensaje</h2>
        </div>
        <div class="col-md-8">
            <section>
                <p>Para más información no dude en contactarse con nosotros.</p>

                <form role="form" method="POST" action="send-message.php">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="InputMessage">Mesagge</label>
                        <textarea class="form-control" id="message" name="message" rows="8"></textarea>
                    </div>
                    <button type="submit" class="btn btn-ar btn-primary">Enviar</button>
                    <div class="clearfix"></div>
                    <br>
                </form>
            </section>
        </div>

        <div class="col-md-4">
            <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-envelope-o"></i> Información adicional</div>
                    <div class="panel-body">
                        <h4 class="section-title no-margin-top">Contacto</h4>
                        <address>
                            <strong>Safe Tech</strong><br>
                            Provincias Unidas 217<br>
                            Rosario, Santa Fe<br>
                            Teléfono: +54 9 3435325148 <br>
                            Mail: <a href="#">info@safe-tech.org</a>
                        </address>

                        <!-- Business Hours -->
                        <h4 class="section-title no-margin-top">Horarios</h4>
                        <ul class="list-unstyled">
                            <li><strong>Lunes-Viernes:</strong> 9 a 17 hs</li>
                            <li><strong>Sábados:</strong> 9 a 14 hs</li>
                            <li><strong>Domingos:</strong> Cerrado</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div> <!-- container -->
