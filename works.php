<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line">Recents Works</h2>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w1.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w2.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w3.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w4.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w5.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="img-caption-ar wow fadeInUp">
                <img src="assets/img/demo/w6.jpg" class="img-responsive" alt="Image">
                <div class="caption-ar">
                    <div class="caption-content">
                        <a href="#" class="animated fadeInDown"><i class="fa fa-search"></i>More info</a>
                        <h4 class="caption-title">Image title</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>