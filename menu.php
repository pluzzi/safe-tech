<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm navbar-dark" href="index.php">Safe <span>Tech</span></a>
        </div> <!-- navbar-header -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="#inicio" data-hover="dropdown">Inicio</a>
                </li>
                <li class="">
                    <a href="#quehacemos" data-hover="dropdown">Que Hacemos</a>
                </li>
                <li class="">
                    <a href="#productos" data-hover="dropdown">Productos y Servicios</a>
                </li>
                <!--<li class="#protfolio">
                    <a href="" data-hover="dropdown">Portfolio</a>
                </li>-->
                <li class="">
                    <a href="#nosotros" data-hover="dropdown">Nosotros</a>
                </li>                
                <li class="">
                    <a href="#planes" data-hover="dropdown">Planes</a>
                </li>
                <li class="">
                    <a href="#equipo" data-hover="dropdown">Nuestro Equipo</a>
                </li>
                <li class="">
                    <a href="#contacto" data-hover="dropdown">Contacto</a>
                </li>
               
            </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>